'''
A program that makes logging even easier
'''
import os
from datetime import datetime
import glob
import logging
from scr import resource_path as rp


def log_setup(FileName):
    '''
    Sets up a folder to store the logs and will ensure there is never more then 10 items in it
    To use LogClass.info(Message)
    '''

    # Creats the name for the logfile based off the datetime on startup
    # The log file will load in the users appdata folder
    log_file = rp.create_resource("{FileName}\\logs".format(FileName=FileName),
                                  "{LogName}.log".format(LogName=str(datetime.now())
                                                         [:-7].replace(":", "-")))

    # Sets the level- info will ignore debug logs but record all others
    log_level = logging.INFO

    # Sets up logging to reconise the file
    logging.basicConfig(filename=log_file,
                        format='%(asctime)s:%(module)s:\n%(levelname)s:%(message)s\n',
                        level=log_level)

    # Tests how long the Logs folder is
    log_files = glob.glob(rp.create_resource(
        "{}".format(FileName), "logs")+"\\*.log")
    # Tests if there are more then 10 log files
    if len(log_files) > 10:
        # Creates a list of the most recent 10 files
        saved_files = sorted(log_files)[-10:]

        # Goes through the files
        for file in log_files:
            # They haven't been saved
            if file not in saved_files:
                # Delete them
                os.remove(file)

    logging.info("Log Setup Successful")

    return logging
