'''
A simple module to handel loading resources in build and from appdata
'''
import sys
import os


def resource_path(relative_path):
    '''
    Gets the absolute path to a resource from its relative path

    Usage: root.wm_iconbitmap(resource_path('icon.ico'))
    '''
    # Tests if the pyinstaller temp file exists
    if hasattr(sys, '_MEIPASS'):
        # If it does return the path to it
        return os.path.join(sys._MEIPASS, relative_path)  # pylint: disable=E1101, W0212
    # Else return the default path to resource
    return os.path.join(os.path.abspath("."), relative_path)


def create_resource(program_name, resource_name):
    '''
    Creates a file in %appdata% and adds the resource to it

    Usage: open(create_resource('myProgram', 'settings.json'))
           **OR** FILE_PATH = create_resource('myProgram', 'settings.json')
              open(FILE_PATH)
    '''
    # Gets the path to programmes appdata file
    dir_path = os.path.join(os.environ["APPDATA"], str(program_name))
    # If the file doesn't exist create it
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    # Return the path to the file- this will work even if the file doesn't exist yet
    return os.path.join(dir_path, str(resource_name))
