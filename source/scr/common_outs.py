'''
This outputs the live party
As this is needed by serveral files it is in its own file
'''


def output_party(party):
    '''
    Output the living party
    '''

    for key in party.keys():
        if party[key]["fagtigued"] == "true":
            continue
        print("\n{Name}".format(Name=key))
        for stat in party[key]:
            if stat == "fagtigued":
                continue
            print("{Stat}: {Value}".format(Stat=stat, Value=party[key][stat]))


def invalid_input(log):
    '''
    Tells the user that their input was invalid
    '''
    print("Sorry but that input was invalid please try again")
    log.warning("Invalid input entered")
