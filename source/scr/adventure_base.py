'''
Loads a new randomly selected quest for the users party
'''
import json
from random import randint, choice
from scr.resource_path import resource_path as rp
from scr import common_outs as co


class NewQuest:
    '''
    Stores the system for hosting quests based off text in a json resourse file
    '''

    def __init__(self, party, log):
        '''
        Selects a quest and runs it
        '''

        self.party = party
        self.log = log

        self.fagtied_character = None

        with open(rp("source\\resources\\adventure_text.json")) as json_file:
            self.story = json.load(json_file)

        print("\nThe adventure begins!- Press enter to continue at any paused point")

        input()

        self.challenge_theme = choice(list(self.story.keys()))

        self.log.info("challenge theme of: {}".format(self.challenge_theme))

        print(choice(self.story[self.challenge_theme]["intro"]))

        input()

        for _i in range(10):
            self.generate_challenge()

            print(self.challenge_text)
            print("{type} challenge level {rating}".format(
                type=self.challenge_type, rating=self.challenge_rating))

            input()

            self.get_character()

            self.run_challenge()

        print(choice(self.story[self.challenge_theme]["outro"]))

        self.fagtied_reset()

    def generate_challenge(self):
        '''
        Produces a random but linked challenge from the json file
        '''
        # Generates the stats for the chalange
        self.challenge_rating = randint(5, 15)
        self.challenge_type = choice(
            ["luck", "magic", "strength", "agility"])

        self.challenge_text = choice(
            self.story[self.challenge_theme][self.challenge_type])

    def get_character(self):
        '''
        Makes the user select a character to complete the challenge
        '''
        print("Please select a party member: ")

        co.output_party(self.party)

        while True:
            self.character_name = input("\nCharacter name: ")

            try:
                self.character = self.party[self.character_name]
                if self.character_name == self.fagtied_character:
                    co.invalid_input(self.log)
                    continue
                self.log.info("selected character: {}".format(self.character))
                break
            except KeyError:
                # If the charater doesn't exist in the party
                co.invalid_input(self.log)

    def run_challenge(self):
        '''
        Runs the base random maths universal to all challenges
        '''
        comp_num = randint(1, 20)
        comp_total = self.challenge_rating + comp_num

        self.log.info("Computer: {}+{}={}".format(comp_num,
                                                  self.challenge_rating, comp_total))

        player_num = randint(1, 20)
        player_total = int(self.character[self.challenge_type]) + player_num

        self.log.info("Player: {}+{}={}".format(player_num,
                                                self.character[self.challenge_type], player_total))

        print("\nComputer rolled {} bringing its total to, {}".format(
            comp_num, comp_total))
        print("You rolled {} bringing {}'s total to {}\n".format(
            player_num, self.character_name, player_total))

        if comp_total > player_total:
            self.loose()
        else:
            self.win()

        if self.fagtied_character:
            # Removes the fagtigue from the previous character
            self.party[self.fagtied_character]["fagtigued"] = "false"
            print("{} is no longer fagtigued".format(self.fagtied_character))

        try:
            # Adds fagtigue to the character
            self.fagtied_character = self.character_name
            self.party[self.character_name]["fagtigued"] = "true"
            print("{} is now fagtigued".format(self.character_name))
        except KeyError:
            # If the character died and no longer exists
            self.fagtied_character = None

        # If there is no or only one character left
        if len(self.party) == 0:  # pylint: disable=len-as-condition
            self.party_death()
        # If there is one character left and they are fagtigued
        elif len(self.party) == 1 and self.fagtied_character:  # pylint: disable=len-as-condition
            print("Your last character is fagtigued and with no one to help them dies")
            self.party_death()

        input()

    def win(self):
        '''
        If the character wins the challenge
        '''
        self.log.info("challenge won!")
        if self.character["hitpoints"] < 4:
            self.log.info("adding hitpoint to {}".format(self.character_name))
            self.character["hitpoints"] += 1

        print("{} successes the challenge! Your party moves forward...\n".format(
            self.character_name))

    def loose(self):
        '''
        If the character looses the challenge
        '''
        self.log.info("challenge LOST :(")

        self.character["hitpoints"] -= 1

        print("{} failed the challenge and lost a life".format(self.character_name))

        message = ""
        if self.character["hitpoints"] <= 0:
            self.log.info("{} died :((".format(self.character_name))
            print("{} has lost all of their lives and died".format(
                self.character_name))
            del self.party[self.character_name]

            self.log.info("party length = {}".format(len(self.party)))

            message = " remaining"

        print("Your{} party moves forward...\n".format(message))

    def fagtied_reset(self):
        '''
        resets the fatigue of all party members to false
        '''
        for character in self.party.keys():
            self.party[character]["fagtigued"] = 'false'

    def party_death(self):
        '''
        To be run if the entire party dies
        '''
        self.log.info("Everyone is dead :( RIP")
        print("The entire party has died.")

        print("Game over...")

        input()

        raise SystemExit
