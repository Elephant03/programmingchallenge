'''
The main file which contains the adventure setup

please note: I have dyslexia and so I apologise for any spelling mistakes in my program
'''
import json
import sqlite3 as lite
from random import randint
from scr import simple_logging
from scr import resource_path as rp
from scr.adventure_base import NewQuest
from scr import common_outs as co


class Adventure:
    '''
    Sets up the advanture
    '''

    def __init__(self):
        # Starts the logs: location- %appdata%/ProgrammingChallenge_appdata/logs
        self.log = simple_logging.log_setup("ProgrammingChallenge_appdata")

        # Where the users parties are stored
        # Found in %appdata%/ProgrammingChallenge_appdata
        self.database_path = rp.create_resource(
            "ProgrammingChallenge_appdata", "adventure.db")

        # To improve validation
        # contains a list of accepted inputs for "yes"
        self.afermatives = ["yes", "1", "true", "sure", "ok",
                            "why not", "continue", "afermative", "y", "not no", "ye", "yea", "yee"
                            "definitly", "tes", "tee", "tea", "t", "okay"]

        self.stats = ["strength", "agility", "magic", "luck"]
        self.party = {}

        self.log.info("Program loaded successfully")

        self.set_up()

        print("\nBehold! Your brave party!")
        co.output_party(self.party)

        NewQuest(self.party, self.log)

        save = input("\nWould you like to save your party? ")

        if save.lower() in self.afermatives:
            self.save_party()

        # To stop it exiting after the last print statment
        input("\nPress enter to exit")

    def set_up(self):
        '''
        The first section of the game to load character creation menu
        '''

        print("""Welcome to your adventure! Would you like to:
1- Create a new party
2- Load a party from file""")

        # Loop incase data is entered incorrectly
        while True:
            choice = input()
            # Loads a new party if true
            if choice.lower() in ["1", "new", "new party", "create",
                                  "create new", "one", "create a new party"]:
                self.load_new_party()
                break
            # Gets a party out of the database in %appdata%/ProgrammingChallenge_appdata
            elif choice.lower() in ["2", "load", "load a party", "two",
                                    "load party", "file", "load from file"]:
                self.get_party()
                break
            else:
                co.invalid_input(self.log)
                continue

        return

    def load_new_party(self):
        '''
        Generates a new party for the game
        '''

        for _i in range(4):
            while True:
                character_name = input(
                    "Please enter a new name for your character: ")

                # Stop name duplications
                if character_name in self.party.keys():
                    co.invalid_input(self.log)
                    continue
                break

            # Adds a dict key to the namea and makes it equal the stats asigned
            self.party[character_name] = self.new_character()

            self.log.info("Character {name} created with stats {stats}".format(
                name=character_name, stats=self.party[character_name]))

            print("Character {} has been created!\n".format(character_name))

        self.log.info("Party constructed {}".format(self.party))

    def new_character(self):
        '''
        Creates a character dictionary
        '''

        # Creats a place to store the player stats
        character_stats = {}

        # This stores a copy of the stats but as atributes are asigned they are deleted
        stats_temp = self.stats.copy()

        # Clears the attribute list
        attributes = []

        # Gets the four character attribute numbers from method
        attributes = [self.character_roll() for _stat in self.stats]

        # Sorts the stats so the lowest can be easily found
        attributes.sort(key=int, reverse=True)

        # Asks if the user wants to reroll there stat
        re_roll = input(
            """Your stat selection is {stats}.
Would you like to re-roll your lowest stat?
The new stat may be lower then present
""".format(stats=", ".join(attributes)))

        if re_roll.lower() in self.afermatives:
            # Removes the old lowest and adds a new stat
            attributes.remove(attributes[-1])
            attributes.append(self.character_roll())

            # Resorts the new stat list
            attributes.sort(key=int, reverse=True)

            print("Your new stats are {stats}".format(
                stats=", ".join(attributes)))

        for num in attributes:
            # If there is only 1 unasigned stat left
            if len(stats_temp) == 1:
                print("Ok asinging {Num} to {Stat}".format(
                    Num=num, Stat=stats_temp[0]))
            else:
                print("What do you want to asign {Num} to: {Stats}".format(
                    Num=num, Stats=", ".join(stats_temp)))

            # Loop to stop invalid inputs
            while True:

                # If there is only 1 unasigned stat left
                if len(stats_temp) == 1:
                    where = stats_temp[0]
                else:
                    where = input()

                # If where matches any strength type
                if where.lower() in ["s", "strength", "str"]:
                    # where is asigned dirctly to strength
                    where = "strength"
                    # ensures strength isn't already asigned
                    if where.lower() not in stats_temp:
                        # if it is reset the loop and display invalid input
                        co.invalid_input(self.log)
                        continue
                elif where.lower() in ["a", "agility", "agl", "agile", "agili"]:
                    where = "agility"
                    if where.lower() not in stats_temp:
                        co.invalid_input(self.log)
                        continue
                elif where.lower() in ["m", "magic", "mag", "ma"]:
                    where = "magic"
                    if where.lower() not in stats_temp:
                        co.invalid_input(self.log)
                        continue
                elif where.lower() in ["l", "luck", "lu"]:
                    where = "luck"
                    if where.lower() not in stats_temp:
                        co.invalid_input(self.log)
                        continue
                else:
                    co.invalid_input(self.log)
                    continue

                # if valid inputs are entered the stat will be asigned the number
                character_stats[where] = num
                # The temperary stats list will remove the asigned stat
                stats_temp.remove(where)
                break

        character_stats["hitpoints"] = 2
        character_stats["fagtigued"] = False

        return character_stats

    def character_roll(self):  # pylint: disable=no-self-use
        '''
        Gets an attribute number for the character stats

        This is based off the greated 3 of 4 random numbers added together
        '''

        # Genetates the four random numbers
        dice = [randint(0, 6) for _i in range(4)]

        # Removes the lowest number
        dice.remove(min(dice))

        # Return the sum of the dices
        return str(sum(dice))

    def get_party(self):
        '''
        Gets the party out of the database
        '''

        while True:
            party_name = input("What was your party's name? ")

            self.log.info("Loading a party: {}".format(party_name))

            with lite.connect(self.database_path) as con:
                cur = con.cursor()

                # Creats the table to store the parties in
                cur.execute("""CREATE TABLE IF NOT EXISTS Parties(
                    Party_Name TEXT PRIMARY KEY,
                    Party TEXT NOT NULL)""")

                try:
                    # Puts in test parties to the database
                    # These are weak parties so it doesn't really matter
                    cur.execute("""INSERT INTO Parties VALUES (
                        "TestParty_full",
                        "{'Steve': {'strength': '1', 'agility': '1', 'magic': '1', 'luck': '1', 'hitpoints': 1, 'fagtigued': 'false'}, 'Alice': {'luck': '1', 'magic': '1', 'agility': '1', 'strength': '1', 'hitpoints': 1, 'fagtigued': 'false'}, 'Bob': {'luck': '1', 'agility': '1', 'magic': '1', 'strength': '1', 'hitpoints': 1, 'fagtigued': 'false'}, 'Evie': {'magic': '1', 'luck': '1', 'strength': '1', 'agility': '1', 'hitpoints': 1, 'fagtigued': 'false'}}"
                    )""")

                    cur.execute("""INSERT INTO Parties VALUES (
                        "TestParty_incomplete",
                        "{'Steve': {'strength': '1', 'agility': '1', 'magic': '1', 'luck': '1', 'hitpoints': 1, 'fagtigued': 'false'}, 'Alice': {'luck': '1', 'magic': '1', 'agility': '1', 'strength': '1', 'hitpoints': 1, 'fagtigued': 'false'}, 'Bob': {'luck': '1', 'agility': '1', 'magic': '1', 'strength': '1', 'hitpoints': 1, 'fagtigued': 'false'}}"
                    )""")

                    cur.execute("""INSERT INTO Parties VALUES (
                        "TestParty_empty",
                        "{}"
                    )""")
                except lite.IntegrityError:
                    # If the already exist they will not run
                    pass

                # Finds the party data
                cur.execute(
                    "SELECT Party FROM Parties WHERE Party_Name = ?", (party_name, ))

                try:
                    # Gets the party data out of the database
                    # uses json.loads to convert database string into dict type
                    self.party = json.loads(
                        cur.fetchall()[0][0].replace("'", "\""))
                    break
                except IndexError as _error:
                    # If the party doesn't exist
                    co.invalid_input(self.log)
                    continue

        self.log.info("Party loaded: {}".format(self.party))

        while len(self.party) < 4:
            self.log.info("Party too small creating new character(s)")
            print("\nYour party is too few!\nCreating a new character")
            while True:
                character_name = input(
                    "Please enter a new name for your character: ")

                # Stop name duplications
                if character_name in self.party.keys():
                    co.invalid_input(self.log)
                    continue
                break

            # Adds a dict key to the namea and makes it equal the stats asigned
            self.party[character_name] = self.new_character()

        return

    def save_party(self):
        '''
        Saves the party to the database
        '''

        self.log.info("Saving party: {}".format(self.party))

        while True:
            party_name = input("Please name your party: ")

            with lite.connect(self.database_path) as con:
                cur = con.cursor()

                # Creats the table if it doesn't already exist
                cur.execute("""CREATE TABLE IF NOT EXISTS Parties(
                    Party_Name TEXT PRIMARY KEY,
                    Party TEXT NOT NULL)""")

                # Tries to fetch the party from the table
                cur.execute("SELECT * FROM Parties WHERE Party_Name=?",
                            (party_name, ))

                if cur.fetchall():
                    # This means the name already exists and will be overwriten
                    over_write = input(
                        "This party already exists.\nAre you sure you wish to overwrite it? ")

                    if over_write.lower() in self.afermatives:
                        # Deletes the old version
                        cur.execute(
                            "DELETE FROM Parties WHERE Party_Name = ?", (party_name, ))
                    else:
                        # Will ask the user to re-name it
                        continue

                # Puts the new party into the database
                cur.execute("INSERT INTO Parties VALUES(?,?)",
                            (party_name, str(self.party)))
            break

        self.log.info("Party saved as {}".format(party_name))

        return


if __name__ == "__main__":

    # Sets the text game when the program is called
    Adventure()
