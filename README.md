# AGS Programming Challenge


A text-based fantacy role-playing game

## To run

On windows 10 run the Adventure!.exe file

On other OS run main.py as a python file using the python idle

For testing:
* Select "Load a party from file" (2)
* Type: "TestParty_full", "TestParty_incomplete" or "TestParty_empty"
* **These parties have min stats so are useles in game**

**All logs and database files are stored in %appdata%/ProgrammingChallenge_appdata/**


### please note: I have dyslexia and so I apologise for any spelling mistakes in my program and lack of adventure text bar proof of concept